var express = require('express');
var router = express.Router();
var mongoUtil = require('../mongoUtil');
var mongo = require('mongodb');
/* GET home page. */
router.get('/', function (req, res, next) {
    res.send("Hello")
});

router.post('/create', function (req, res, next) {
    var db = mongoUtil.getDb();
    let data = db.collection('comments');
    let currentTime = new Date().getTime();
    let dataToStore = { name: "Rishi", comment: req.body.comment, level: 0, parent:null, created_at: currentTime, updated_at: currentTime }
    data.insertOne(dataToStore, function(err, res) {
        if(err) throw err;
    });
    res.redirect('/');
});

router.get('/comments/:id', function (req, res, next) {
    var db = mongoUtil.getDb();
    let data = db.collection('comments');
    var id = req.params.id;
    findLevelComments(data, id, function(result){
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(result));
    });
});

function findLevelComments(data, parentID, callback) {
    var o_id = new mongo.ObjectID(parentID);
    data.find({ "parent": o_id }).toArray(function (err, docs) {
        callback(docs);
    });
}

module.exports = router;