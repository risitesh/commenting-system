var express = require('express');
var router = express.Router();
var mongoUtil = require('../mongoUtil');

/* GET home page. */
router.get('/', function(req, res, next) {
  var db = mongoUtil.getDb();
  let dbData = db.collection('comments');
  findComments(dbData, function(data) {
    findLevelComments(dbData, function (result) {
      res.render('index', { title: "Commenting System", data: data, level1: result });
    })
  })
});

function findComments(data,callback) {
  data.find({"parent": null}).toArray(function (err, docs) {
    callback(docs);
  });
}

function findLevelComments(data, callback) {
  data.find({ "parent": {$ne : null}, "level": 1 }).toArray(function (err, docs) {
    callback(docs);
  });
}

module.exports = router;