const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const url = 'mongodb://localhost:27017';
var _db;

module.exports = {
    connectToServer: function (callback) {
        MongoClient.connect(url, function (err, client) {
            assert.equal(null, err);
            _db = client.db('test_db');
            return callback(err);
        });
    },
    getDb: function () {
        return _db;
    }
};